// Generated by Selenium IDE
import helpers.WebDriverFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.*;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class GeneralTest {
  JavascriptExecutor js;
  private WebDriver driver;
  private Map<String, Object> vars;

  @Before
  public void setUp() {
    driver = WebDriverFactory.getDriver();

    driver.get("http://www.gdansk.pl/");
    driver.manage().addCookie(new Cookie.Builder("rodo", "true")
            .path("/")
            .build());
    driver.get("http://www.gdansk.pl/");

    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }

  @After
  public void tearDown() {
    driver.quit();
  }

  @Test
  public void testPresidentName() {
    driver.findElement(By.xpath("//node()[contains(text(),\"Urząd Miejski:\")]")).click();
    List<WebElement> elements = driver.findElements(By.xpath("//a[contains(text(), \"Prezydent Miasta Gdańska\")]/ancestor::div//h3[text()=\"Aleksandra Dulkiewicz\"]"));
    Assert.assertTrue (elements.size() >= 1 );
  }

  @Test
  public void mainPageMenu() {
    {
      List<WebElement> elements = driver.findElements(By.xpath("//li[2]/a[contains(text(), \"Wiadomości\")]"));
      assert (elements.size() > 0);
    }
    {
      List<WebElement> elements = driver.findElements(By.xpath("//li[1]/a[contains(text(), \"Załatw sprawę\")]"));
      assert (elements.size() > 0);
    }
    {
      List<WebElement> elements = driver.findElements(By.xpath("//li[6]/a[contains(text(), \"Gdańskie Centrum Kontaktu\")]"));
      assert (elements.size() > 0);
    }
  }

  @Test
  public void calendarEvents() {
    {
      List<WebElement> elements = driver.findElements(By.id("button_dzien"));
      assert (elements.size() > 0);
    }
    {
      List<WebElement> elements = driver.findElements(By.id("button_miesiac"));
      assert(elements.size() > 0);
    }
  }

  @Test
  public void servicesSection() {
    {
      List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"footer\"]/footer/div/div[3]/ul/li[7]/a"));
      assert (elements.size() > 0);
    }
    {
      List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"footer\"]/footer/div/div[3]/ul/li[25]/a"));
      assert (elements.size() > 0);
    }
    {
      List<WebElement> elements = driver.findElements(By.xpath("//*[@id=\"footer\"]/footer/div/div[3]/ul/li[11]/a"));
      assert (elements.size() > 0);
      assertThat(driver.findElement(By.linkText("VAT Centralny")).getText(), is("VAT Centralny"));
    }
  }

  @Test
  public void pageNews() {
    driver.findElement(By.linkText("Wiadomości")).click();
    {
      List<WebElement> elements = driver.findElements(By.xpath("/html/body/wrapper/main/div[3]/div/div[1]/div/ul"));
      assert (elements.size() != 0);
    }
    {
      List<WebElement> elements = driver.findElements(By.xpath("/html/body/wrapper/main/div[3]/div/div[2]/div"));
      assert (elements.size() > 0);
    }
  }

}
